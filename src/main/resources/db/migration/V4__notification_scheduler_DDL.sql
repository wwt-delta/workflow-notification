-- Dumping structure for table wfengine.client
CREATE TABLE IF NOT EXISTS `emailnotification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_name` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `workflowid` varchar(255) NOT NULL,
  `no_of_emails_sent` int(11) DEFAULT 0,
  `last_email_sent_datetime` datetime DEFAULT NULL,
  `transactiontype` varchar(255) NOT NULL,
  `actiontype` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;