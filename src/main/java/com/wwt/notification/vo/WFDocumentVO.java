package com.wwt.notification.vo;

import java.util.Date;

public class WFDocumentVO {

	private String workflowId;
	private String application;
	private String clientId;
	private int priority;
	private String transactionType;
	private String previousAction;
	private Date createdDate;
	private int recurringMailNotification;
	
	public String getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getPreviousAction() {
		return previousAction;
	}

	public void setPreviousAction(String previousAction) {
		this.previousAction = previousAction;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getRecurringMailNotification() {
		return recurringMailNotification;
	}

	public void setRecurringMailNotification(int recurringMailNotification) {
		this.recurringMailNotification = recurringMailNotification;
	}

	public WFDocumentVO(String workflowId, String application, String clientId, int priority, String transactionType,
			String previousAction, Date createdDate, int recurringMailNotification) {
		super();
		this.workflowId = workflowId;
		this.application = application;
		this.clientId = clientId;
		this.priority = priority;
		this.transactionType = transactionType;
		this.previousAction = previousAction;
		this.createdDate = createdDate;
		this.recurringMailNotification = recurringMailNotification;
	}

	public WFDocumentVO() {
		super();
	}
	

}