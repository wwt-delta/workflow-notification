package com.wwt.notification.vo;

import java.util.Date;

import com.wwt.notification.util.enums.WorkFlowActionType;

public class NotificationEmailVO {
	
	private String previousAction;
	private String workflowId;
	private int priority;
	private String serverUrl;
	private String applicationName;
	private WorkFlowActionType actionType;
	private int noOfEmailsSent;
	private Date lastEmailTriggeredTime;
	
	
	public Date getLastEmailTriggeredTime() {
		return lastEmailTriggeredTime;
	}
	public void setLastEmailTriggeredTime(Date lastEmailTriggeredTime) {
		this.lastEmailTriggeredTime = lastEmailTriggeredTime;
	}
	public String getPreviousAction() {
		return previousAction;
	}
	public void setPreviousAction(String previousAction) {
		this.previousAction = previousAction;
	}
	public String getWorkflowId() {
		return workflowId;
	}
	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public String getServerUrl() {
		return serverUrl;
	}
	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public WorkFlowActionType getActionType() {
		return actionType;
	}
	public void setActionType(WorkFlowActionType actionType) {
		this.actionType = actionType;
	}
	public int getNoOfEmailsSent() {
		return noOfEmailsSent;
	}
	public void setNoOfEmailsSent(int noOfEmailsSent) {
		this.noOfEmailsSent = noOfEmailsSent;
	}

}
