package com.wwt.notification.model;

public class WFTransactionType {
	
	private String transactionType;
	private int recurringMailNotifyTime;
	private String clientId;
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public int getRecurringMailNotifyTime() {
		return recurringMailNotifyTime;
	}
	public void setRecurringMailNotifyTime(int recurringMailNotifyTime) {
		this.recurringMailNotifyTime = recurringMailNotifyTime;
	}
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public WFTransactionType(String transactionType, int recurringMailNotifyTime, String clientId) {
		this.transactionType = transactionType;
		this.recurringMailNotifyTime = recurringMailNotifyTime;
		this.clientId = clientId; 
	}
	public WFTransactionType() {
		super();
	}
	

}
