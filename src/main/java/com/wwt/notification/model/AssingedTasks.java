package com.wwt.notification.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.wwt.notification.util.enums.WorkFlowStatus;

public class AssingedTasks {
	private String assignedto;
	private String  completedby;
	private String 	 display;
	private String tasktype;
	private String workflowId;
	private String application;
	private String taskId;
	private String clientId;
	
	private int priority;
	
	private String transactionType;
	
	private String referenceId;
	
	private String referenceUrl;
	
	private String requestedFor;
	
	private String requestedBy;
	
	private String summary;
	
	private String description;
	
	private String status;
	
	private Date createdDate;
	
	private String createdBy;
	
	private Date modifiedDate;
	
	private String modifiedBy;
	
	private String postCompleteUrl;
	
	private String postCancelUrl;
	
	private String postCompletePayload;
	
	private String postCancelPayload;
	
	private boolean hideForRequestedFor;
	
	private boolean hideForRequestedBy;
	
	private String previousAction;
	
	
	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getAssignedto() {
		return assignedto;
	}

	public void setAssignedto(String assignedto) {
		this.assignedto = assignedto;
	}

	public String getCompletedby() {
		return completedby;
	}

	public void setCompletedby(String completedby) {
		this.completedby = completedby;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getTasktype() {
		return tasktype;
	}

	public void setTasktype(String tasktype) {
		this.tasktype = tasktype;
	}

	public String getPreviousAction() {
		return previousAction;
	}

	public void setPreviousAction(String previousAction) {
		this.previousAction = previousAction;
	}

	public String getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getReferenceUrl() {
		return referenceUrl;
	}

	public void setReferenceUrl(String referenceUrl) {
		this.referenceUrl = referenceUrl;
	}

	public String getRequestedFor() {
		return requestedFor;
	}

	public void setRequestedFor(String requestedFor) {
		this.requestedFor = requestedFor;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedby) {
		this.requestedBy = requestedby;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getPostCompleteUrl() {
		return postCompleteUrl;
	}

	public void setPostCompleteUrl(String postCompleteUrl) {
		this.postCompleteUrl = postCompleteUrl;
	}

	public String getPostCancelUrl() {
		return postCancelUrl;
	}

	public void setPostCancelUrl(String postCancelUrl) {
		this.postCancelUrl = postCancelUrl;
	}

	public String getPostCompletePayload() {
		return postCompletePayload;
	}

	public void setPostCompletePayload(String postCompletePayload) {
		this.postCompletePayload = postCompletePayload;
	}

	public String getPostCancelPayload() {
		return postCancelPayload;
	}

	public void setPostCancelPayload(String postCancelPayload) {
		this.postCancelPayload = postCancelPayload;
	}

	public boolean isHideForRequestedFor() {
		return hideForRequestedFor;
	}

	public void setHideForRequestedFor(boolean hideForRequestedFor) {
		this.hideForRequestedFor = hideForRequestedFor;
	}

	public boolean isHideForRequestedBy() {
		return hideForRequestedBy;
	}

	public void setHideForRequestedBy(boolean hideForRequestedBy) {
		this.hideForRequestedBy = hideForRequestedBy;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "AssingedTasks [assignedto=" + assignedto + ", completedby=" + completedby + ", display=" + display
				+ ", tasktype=" + tasktype + ", workflowId=" + workflowId + ", application=" + application + ", taskId="
				+ taskId + ", clientId=" + clientId + ", priority=" + priority + ", transactionType=" + transactionType
				+ ", referenceId=" + referenceId + ", referenceUrl=" + referenceUrl + ", requestedFor=" + requestedFor
				+ ", requestedBy=" + requestedBy + ", summary=" + summary + ", description=" + description + ", status="
				+ status + ", createdDate=" + createdDate + ", createdBy=" + createdBy + ", modifiedDate="
				+ modifiedDate + ", modifiedBy=" + modifiedBy + ", postCompleteUrl=" + postCompleteUrl
				+ ", postCancelUrl=" + postCancelUrl + ", postCompletePayload=" + postCompletePayload
				+ ", postCancelPayload=" + postCancelPayload + ", hideForRequestedFor=" + hideForRequestedFor
				+ ", hideForRequestedBy=" + hideForRequestedBy + ", previousAction=" + previousAction + "]";
	}

	

}
