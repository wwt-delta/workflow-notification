package com.wwt.notification.model;

import java.io.Serializable;
import java.util.Date;

public class WFDocument implements Serializable {


	private String workflowId;
	
	private String application;
	
	private String clientId;
	
	private int priority;
	
	private String transactionType;
	
	private String referenceId;
	
	private String referenceUrl;
	
	private String requestedFor;
	
	private String requestedBy;
	
	private String summary;
	
	private String description;
	
	private Date createdDate;
	
	private String createdBy;
	
	private Date modifiedDate;
	
	private String modifiedBy;
	
	private String postCompleteUrl;
	
	private String postCancelUrl;
	
	private String postCompletePayload;
	
	private String postCancelPayload;
	
	private boolean hideForRequestedFor;
	
	private boolean hideForRequestedBy;
	
	private String previousAction;
	
	private int recurringNotificationTime;

	public String getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getReferenceUrl() {
		return referenceUrl;
	}

	public void setReferenceUrl(String referenceUrl) {
		this.referenceUrl = referenceUrl;
	}

	public String getRequestedFor() {
		return requestedFor;
	}

	public void setRequestedFor(String requestedFor) {
		this.requestedFor = requestedFor;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getPostCompleteUrl() {
		return postCompleteUrl;
	}

	public void setPostCompleteUrl(String postCompleteUrl) {
		this.postCompleteUrl = postCompleteUrl;
	}

	public String getPostCancelUrl() {
		return postCancelUrl;
	}

	public void setPostCancelUrl(String postCancelUrl) {
		this.postCancelUrl = postCancelUrl;
	}

	public String getPostCompletePayload() {
		return postCompletePayload;
	}

	public void setPostCompletePayload(String postCompletePayload) {
		this.postCompletePayload = postCompletePayload;
	}

	public String getPostCancelPayload() {
		return postCancelPayload;
	}

	public void setPostCancelPayload(String postCancelPayload) {
		this.postCancelPayload = postCancelPayload;
	}

	public boolean isHideForRequestedFor() {
		return hideForRequestedFor;
	}

	public void setHideForRequestedFor(boolean hideForRequestedFor) {
		this.hideForRequestedFor = hideForRequestedFor;
	}

	public boolean isHideForRequestedBy() {
		return hideForRequestedBy;
	}

	public void setHideForRequestedBy(boolean hideForRequestedBy) {
		this.hideForRequestedBy = hideForRequestedBy;
	}

	public String getPreviousAction() {
		return previousAction;
	}

	public void setPreviousAction(String previousAction) {
		this.previousAction = previousAction;
	}

	public int getRecurringNotificationTime() {
		return recurringNotificationTime;
	}

	public void setRecurringNotificationTime(int recurringNotificationTime) {
		this.recurringNotificationTime = recurringNotificationTime;
	}

	public WFDocument() {
		super();
	}
	
	
}
