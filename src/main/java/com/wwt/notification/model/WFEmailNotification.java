package com.wwt.notification.model;

import java.util.Date;

import com.wwt.notification.util.enums.WorkFlowActionType;

public class WFEmailNotification {

	private String previousAction;
	private String workflowId;
	private int priority;
	private String clientId;
	private String serverUrl;
	private String applicationName;
	private String actionType;
	private int noOfEmailsSent;
	private Date lastEmailTriggeredTime;
	private String transactionType;
	private int id;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public Date getLastEmailTriggeredTime() {
		return lastEmailTriggeredTime;
	}
	public void setLastEmailTriggeredTime(Date lastEmailTriggeredTime) {
		this.lastEmailTriggeredTime = lastEmailTriggeredTime;
	}
	public String getPreviousAction() {
		return previousAction;
	}
	public void setPreviousAction(String previousAction) {
		this.previousAction = previousAction;
	}
	public String getWorkflowId() {
		return workflowId;
	}
	public void setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public String getServerUrl() {
		return serverUrl;
	}
	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public int getNoOfEmailsSent() {
		return noOfEmailsSent;
	}
	public void setNoOfEmailsSent(int noOfEmailsSent) {
		this.noOfEmailsSent = noOfEmailsSent;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	
}
