package com.wwt.notification.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.wwt.notification.json.WFNotificationServiceJson;
import com.wwt.notification.model.AssingedTasks;
import com.wwt.notification.vo.NotificationEmailVO;
import com.wwt.notification.vo.WFNotificationVO;

/**
 * EMailUtility processes the triggering email functionality through the Notification server.
 * @author Delta
 *
 */
@Component
public class EmailUtility {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private RestTemplate restTemplate = new RestTemplate();
	
	private String messageHeader = "Please check the status of Workflow document and its tasks below";

	public int doSendMailNotification(WFNotificationVO notificationVO, String serverUrl)
	{

		List<String> bccList = new ArrayList<String>();

		WFNotificationServiceJson notificationJson = new WFNotificationServiceJson();
		notificationJson.setApplicationName(notificationVO.getApplicationName());
		notificationJson.setTO(notificationVO.getLiMailIds());
		notificationJson.setBCC(bccList);
		notificationJson.setCC(bccList);
		notificationJson.setFrom(notificationVO.getUserId());
		notificationJson.setPriority(notificationVO.getPriority());
		notificationJson.setMessage(notificationVO.getBodyContent());
		notificationJson.setSubject(notificationVO.getSubject());
		notificationJson.setRetry(false);
		notificationJson.setRetryCount(0);
		int statusCode = 0;
		ResponseEntity<String> response = null;
		System.out.println("Emailutitlity--"+notificationJson);
		try
		{
			MultiValueMap<String, String> headerMap = new LinkedMultiValueMap<String, String>();
			headerMap.add("Content-Type", "application/json");
			headerMap.add("Accept", "application/json");
			HttpEntity<WFNotificationServiceJson> requestEntity = new HttpEntity<WFNotificationServiceJson>(notificationJson, headerMap);
			ResponseEntity<EmailResponse> resultResponseEntity = restTemplate.exchange(serverUrl, HttpMethod.POST,
					requestEntity, EmailResponse.class);
			System.out.println("Before sending the notification content" + notificationVO.getBodyContent());
			statusCode = resultResponseEntity.getStatusCodeValue();
			logger.debug("Notification server response code " + statusCode);
		}catch(HttpClientErrorException ex)
		{
			System.out.println("http error :: "+ex.getMessage());
			System.out.println("http error :: "+ex.getResponseBodyAsString());
			ex.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			handleCustomException(statusCode);
		}
		System.out.println("Exit from the notification task interface...");
		return statusCode;
	}
	
	private void handleCustomException(int statusCode)
	{
		switch (statusCode)
		{
		case 400:
			logger.error("Bad Request .raised Please correct the request parameters and try again." + statusCode);
			break;
		case 401:
			logger.error("Unauthoraised Request raised. Please check with the administrator." + statusCode);
			break;
		case 403:
			logger.error("The request was valid, but the server is refusing action. " + statusCode);
			break;
		case 404:
			logger.error("Notification server is unavailable. This interface shall retry connecting the Notification server to send Notifications." + statusCode);
			break;
		case 405:
			logger.error("A request method is not supported for the requested resource." + statusCode);
			break;
		case 408:
			logger.error("The server timed out waiting for the request." + statusCode);
			break;
		case 500:
			logger.error("Internal server issue. Please check with the administrator" + statusCode);
			break;
		case 503:
			logger.error("Service unavailable." + statusCode);
			break;
		default :
			logger.error("Unknown error occurred. Status code returned is " + statusCode);
			break;
		}
	}//end of handleCustomerException method
	
	public int processEmailNotification(List<AssingedTasks> tasksList, NotificationEmailVO emailVO)
	{
		StringBuilder mailContent = new StringBuilder("<html> <body>");
		int statusCode = 0;
		String taskIds = null;
		String taskStatus = null;
		String userId = "admin@wwt.com";

		List<String> approverEmails = new ArrayList<String>();
		for(AssingedTasks taskDoc : tasksList)
		{
			if(taskIds == null)
			{
				taskIds = taskDoc.getTasktype();
			}
			else
			{
				taskIds = taskIds + "," + taskDoc.getTaskId();
			}
			approverEmails.add(taskDoc.getAssignedto());
			taskStatus = taskDoc.getStatus();
		}

		if(taskIds.contains(","))
		{
			taskIds = taskIds + " are";
		}
		else
		{
			taskIds = taskIds + " is";
		}
		
		StringBuilder bodyContent = new StringBuilder(taskIds).append(" Pending for ");

		switch (emailVO.getActionType())
		{
		case REQUEST_APPROVAL :
			bodyContent.append("Approvals.");
			break;
		case REVIEW :
			bodyContent.append("Review.");
			break;
		case ASSIGN:
			bodyContent.append("Approvals.");
			break;
		default:
			bodyContent.append("Approvals.");
			break;
		}

		mailContent.append("<h3>").append(messageHeader).append("</h3>").append("<h4>").append("Task Details : ")
				.append(bodyContent).append("</h4>").append("<h4> Task Status : ").append(taskStatus).append("</h4>")
				.append("<h4> Number of Email Notifications sent : ").append(emailVO.getNoOfEmailsSent())
				.append("</h4>").append("</body> </html>");
	
		
		if(approverEmails != null && approverEmails.size() > 0)
		{
			String subject = "Workflow Document " + emailVO.getWorkflowId() + " is Active. Current Task Status is " + emailVO.getPreviousAction();

			WFNotificationVO notificationVO = new WFNotificationVO();
			notificationVO.setApplicationName(emailVO.getApplicationName());
			notificationVO.setLiMailIds(approverEmails);
			notificationVO.setPriority(emailVO.getPriority());
			notificationVO.setUserId(userId);
			notificationVO.setBodyContent(mailContent.toString());
			notificationVO.setSubject(subject);
			notificationVO.setRetry(false);
			notificationVO.setRetryCount(0);
			statusCode = doSendMailNotification(notificationVO, emailVO.getServerUrl());
		}
		return statusCode;
	}
}//end of the class
class EmailResponse{
	private static final long serialVersionUID = 1L;
	List<String> message;

	public List<String> getMessage() {
		return message;
	}

	public void setMessage(List<String> message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "EmailResponse [message=" + message + "]";
	}
	
	
}