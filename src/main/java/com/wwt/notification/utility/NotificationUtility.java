package com.wwt.notification.utility;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.wwt.notification.model.AssingedTasks;
import com.wwt.notification.service.NotificationService;
import com.wwt.notification.util.enums.WorkFlowActionType;
import com.wwt.notification.vo.NotificationEmailVO;
import com.wwt.notification.vo.WFDocumentVO;

/**
 * 
 * @author Delta Technology
 *
 */
@Component
//@EnableConfigurationProperties
@PropertySource("classpath:application.yml")
public class NotificationUtility {
	
	@Autowired
	private NotificationService notificationService;
	
	@Value("${notification.mail.priority1}")
	private String PRIORITY_TIME_1;

	@Value("${notification.mail.priority2}")
	private String PRIORITY_TIME_2;
	
	/*@Value("${notification.server.name}")
	private String NOTIFICATION_SERVER_NAME;

	@Value("${notification.server.port}")
	private String NOTIFICATION_SERVER_PORT;

	@Value("${notification.server.context}")
	private String NOTIFICATION_SERVER_CONTEXT;*/
	
	@Value("${notification.server.url}")
	private String NOTIFICATION_SERVER_URL;
	
	/** Time in seconds every 1 hour */
	private int timeInSeconds = 3600;

	@Autowired
	private EmailUtility emailUtility;
	
	private String serverUrl = null;
	
	@Bean
	private String getServerUrl()
	{
		return NOTIFICATION_SERVER_URL;
	}

	/**
	 * 
	 * @param currTimeInMillSecs
	 * @param lastMailSentTimeInMillsec
	 * @param priority
	 * @return
	 */
	public boolean isMailToSend(long currTimeInMillSecs, long lastMailSentTimeInMillsec, int priority)
	{
		boolean mailFlag = false;
		long timDiffInSeconds = (currTimeInMillSecs - lastMailSentTimeInMillsec) / 1000;

		switch (priority)
		{
		case 1:
			int priority1 = Integer.parseInt(PRIORITY_TIME_1) * timeInSeconds;
			mailFlag = timDiffInSeconds >= priority1  ? true : false;
			break;
		case 2:
			int priority2 = Integer.parseInt(PRIORITY_TIME_2) * timeInSeconds;
			mailFlag = timDiffInSeconds >= priority2 ? true : false;
			break;
		default:
			System.out.println("As a low priority document no email notifications are configured.");
			break;
		}
		return mailFlag;
	}
	
	/**
	 * To prepare the mail content.
	 * 
	 * @param doc - WorkFlow document corresponding to the selected transaction type
	 * @param noOfMailsSent
	 */
	public int triggerEmailUtility(WFDocumentVO doc, int noOfMailsSent, String taskType)
	{
		List<AssingedTasks> tasksList = notificationService.retrieveActiveWFTasks(doc.getWorkflowId(), WorkFlowActionType.valueOf(taskType));
		int status = 0;
		if(tasksList != null && tasksList.size() > 0)
		{
			System.out.println("Server URL---->"+getServerUrl());
			NotificationEmailVO emailVO = new NotificationEmailVO();
			emailVO.setApplicationName(doc.getApplication());
			emailVO.setPreviousAction(doc.getPreviousAction());
			emailVO.setPriority(doc.getPriority());
			emailVO.setServerUrl(getServerUrl());
			emailVO.setActionType(WorkFlowActionType.valueOf(taskType));
			emailVO.setNoOfEmailsSent(noOfMailsSent);
			
			status = emailUtility.processEmailNotification(tasksList, emailVO);
			
		}
		return status;
	}
}
