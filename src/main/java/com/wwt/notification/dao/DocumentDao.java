package com.wwt.notification.dao;

import java.util.List;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.wwt.notification.vo.WFDocumentVO;
@RepositoryRestResource(collectionResourceRel = "wfDocument", path = "wfDocument")
/**
 * Gets all active workflow documents.
 * @return list of WF Document objects
 */
public interface DocumentDao{
		List<WFDocumentVO> findActiveWFDocuments();
}