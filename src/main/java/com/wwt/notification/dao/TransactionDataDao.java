package com.wwt.notification.dao;

import java.util.List;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.wwt.notification.model.WFTransactionType;

@RepositoryRestResource(collectionResourceRel = "transaction_type", path = "transaction_type")
public interface TransactionDataDao {
	
	/**
	 * Gets all transactions for only registered clients
	 * 
	 * @return List of transaction objects
	 */
	public List<WFTransactionType> getAllValidTransactions();
}