package com.wwt.notification.dao;

import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.wwt.notification.model.WFEmailNotification;
import com.wwt.notification.vo.NotificationEmailVO;

@RepositoryRestResource(collectionResourceRel = "emailNotification", path = "emailNotification")
public interface EmailNotificationDao {
	/**
	 * Gets all notification details different actiontypes.
	 * @return  WF Notification objects
	 */
	public WFEmailNotification findEmailNotification(String workflowId,  String actionType);
}
