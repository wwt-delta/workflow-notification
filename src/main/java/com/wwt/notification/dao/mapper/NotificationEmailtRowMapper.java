package com.wwt.notification.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wwt.notification.model.WFEmailNotification;
import com.wwt.notification.vo.NotificationEmailVO;



public class NotificationEmailtRowMapper implements RowMapper {

	@Override
	public WFEmailNotification mapRow(ResultSet rs, int arg1) throws SQLException {
		WFEmailNotification notification = new WFEmailNotification();
		notification.setId(rs.getInt("id"));
		notification.setWorkflowId(rs.getString("workflowId"));
		notification.setNoOfEmailsSent(rs.getInt("no_of_emails_sent"));
		notification.setApplicationName(rs.getString("application_name"));
		notification.setActionType(rs.getString("actiontype"));
		notification.setLastEmailTriggeredTime(rs.getDate("last_email_sent_datetime"));
		notification.setApplicationName(rs.getString("application_name"));
		notification.setClientId(rs.getString("client_id"));
		notification.setTransactionType(rs.getString("transactiontype"));
		return notification;
	}

}
