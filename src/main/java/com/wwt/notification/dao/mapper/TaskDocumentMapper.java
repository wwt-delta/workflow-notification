package com.wwt.notification.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wwt.notification.model.AssingedTasks;
import com.wwt.notification.util.enums.WorkFlowStatus;
import com.wwt.notification.vo.WFDocumentVO;


public class TaskDocumentMapper implements RowMapper<AssingedTasks> {

	@Override
	public AssingedTasks mapRow(ResultSet rs, int arg1) throws SQLException {
		AssingedTasks document = new AssingedTasks();
		document.setWorkflowId(rs.getString("workflowid"));
		document.setClientId(rs.getString("clientid"));
		document.setAssignedto(rs.getString("assignedto"));
		document.setCompletedby(rs.getString("completedby"));
		document.setCreatedBy(rs.getString("createdby"));
		document.setDisplay(rs.getString("display"));
		document.setModifiedBy(rs.getString("modifiedby"));
		document.setModifiedDate(rs.getDate("modifieddate"));
		document.setPostCancelPayload(rs.getString("post_cancel_url_payload"));
		document.setStatus(rs.getString("status"));
		document.setTaskId(rs.getString("taskid"));
		document.setTasktype(rs.getString("tasktype"));
		return document;
	}

}
