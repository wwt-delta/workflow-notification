package com.wwt.notification.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wwt.notification.model.WFTransactionType;


public class TranactionTypeRowMapper implements RowMapper {

	@Override
	public WFTransactionType mapRow(ResultSet rs, int arg1) throws SQLException {
		WFTransactionType txn = new WFTransactionType();
		txn.setTransactionType(rs.getString("transaction_type"));
		txn.setRecurringMailNotifyTime(rs.getInt("recurring_email_notification"));
		txn.setClientId(rs.getString("client_ref_id"));
		return txn;
	}

}
