package com.wwt.notification.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wwt.notification.vo.WFDocumentVO;


public class DocumentRowMapper implements RowMapper {

	@Override
	public WFDocumentVO mapRow(ResultSet rs, int arg1) throws SQLException {
		WFDocumentVO document = new WFDocumentVO();
		document.setWorkflowId(rs.getString("workflowid"));
		document.setApplication(rs.getString("application"));
		document.setClientId(rs.getString("client_id"));
		document.setPriority(rs.getInt("priority"));
		document.setTransactionType(rs.getString("transactiontype"));
		document.setPreviousAction(rs.getString("previous_action"));
		document.setCreatedDate(rs.getDate("createddate"));
		document.setRecurringMailNotification(rs.getInt("recurring_email_notification"));
		System.out.println("DocumentRowMapper**********"+document);
		return document;
	}

}
