package com.wwt.notification.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.wwt.notification.dao.EmailNotificationDao;
import com.wwt.notification.dao.mapper.NotificationEmailtRowMapper;
import com.wwt.notification.model.WFEmailNotification;


@Repository("emailNotifcRepo")
@RepositoryRestResource(collectionResourceRel = "emailNotification", path = "emailNotification")
public class EmailNotificationDaoImpl implements EmailNotificationDao{
	@Autowired
	JdbcTemplate jdbcTemplate;
	//@Query(value = "select notification from EmailNotification notification where notification.workflowId = :workflowId and notification.actionType = :actionType")
	/**
	 * Gets all notification details different actiontypes.
	 * @return  WF Notification objects
	 */
	public WFEmailNotification findEmailNotification( String workflowId,String actionType) {
		String sql = "select * from emailnotification nf where nf.workflowid =? and nf.actiontype =?";
		List<WFEmailNotification> list=new ArrayList<>();
		WFEmailNotification notication=null;
		list =  jdbcTemplate.query(sql, new Object[]{workflowId,actionType}, new NotificationEmailtRowMapper());
		if(list!=null && list.size() > 0){
			notication=list.get(0);
		}
	
		return notication;
	}
	

	
}
