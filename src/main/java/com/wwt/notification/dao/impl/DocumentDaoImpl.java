package com.wwt.notification.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.wwt.notification.dao.DocumentDao;
import com.wwt.notification.dao.mapper.DocumentRowMapper;
import com.wwt.notification.vo.WFDocumentVO;


@RepositoryRestResource(collectionResourceRel = "wfDocument", path = "wfDocument")
@Repository("documentDao")
public class DocumentDaoImpl implements DocumentDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	@Override
	/**
	 * Gets all active workflow documents.
	 * @return list of WF Document objects
	 */
	public List<WFDocumentVO> findActiveWFDocuments() {
		String sql = "select wfd.workflowid, wfd.application, wfd.client_id, wfd.priority, wfd.transactiontype,wfd.previous_action, wfd.createddate, tt.recurring_email_notification from workflowdocument wfd, client cl, transaction_type tt where wfd.client_id = cl.client_id and cl.client_status = 1 and wfd.transactiontype = tt.transaction_type and tt.client_ref_id = cl.client_id and wfd.status IN ('ACTIVE', 'NEEDS_APPROVAL', 'ASSIGNED') and wfd.priority in (1,2) and tt.initial_email_notification = 1";
		List<WFDocumentVO> document = jdbcTemplate.query(sql, new DocumentRowMapper());
		System.out.println("**findActiveWFDocuments*******"+document);
		return document;

	}

}