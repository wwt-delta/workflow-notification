package com.wwt.notification.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.wwt.notification.dao.TaskDocumentDao;
import com.wwt.notification.dao.mapper.TaskDocumentMapper;
import com.wwt.notification.model.AssingedTasks;
import com.wwt.notification.util.enums.WorkFlowActionType;


@RepositoryRestResource(collectionResourceRel = "wfTaskDocument", path = "wfTaskDocument")
@Repository("taskRepo")
public class TaskDocumentDaoImpl implements TaskDocumentDao
{
	@Autowired
	JdbcTemplate jdbcTemplate;
	@SuppressWarnings("unchecked")
	/**
	 * This will get all the list of assigned tasks
	 * @param workflowId
	 * @param actionType
	 * @return list of AssignedTasks objects.
	 */
	public List<AssingedTasks> getListOfActiveTasks(String workflowId, WorkFlowActionType actionType){
		String sql = "select * from workflowtask wft,workflowdocument wd where wd.workflowid=wft.workflowid and wft.status='ACTIVE' and wd.workflowid = ?  and wft.createddate is not null and wft.tasktype = ? and wft.assignedto like '%%'";
		List<AssingedTasks> document ;
		System.out.println("TaskDocumentDaoImpl-workflowId--->"+workflowId+"***actionType"+actionType);
		document = jdbcTemplate.query(sql,  new Object[]{workflowId,actionType.toString()},new TaskDocumentMapper());
		return document;
		
	}
}

