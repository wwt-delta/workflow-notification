package com.wwt.notification.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.wwt.notification.dao.TransactionDataDao;
import com.wwt.notification.dao.mapper.TranactionTypeRowMapper;
import com.wwt.notification.model.WFTransactionType;


@RepositoryRestResource(collectionResourceRel = "transaction_type", path = "transaction_type")
@Repository("transRepo")
public class TransactionDataDaoImpl implements TransactionDataDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	/**
	 * Gets all transactions for only registered clients
	 * 
	 * @return List of transaction objects
	 */
	/*@Query(value = "select new com.wwt.notification.vo.TransactionTypeVO(trans.transactionType, trans.recurringNotificationTime, trans.client.clientId) "
			+ "from TransactionTypeTable trans, ClientTable cli where trans.client.clientId = cli.clientId and trans.initialNotificationTime > 0")*/
	public List<WFTransactionType> getAllValidTransactions(){
		String sql = "select tt.transaction_type ,tt.recurring_email_notification, tt.client_ref_id from transaction_type tt, client cl where tt.client_ref_id = cl.clientId and tt.initial_email_notification > 0";
		List<WFTransactionType> txnType = jdbcTemplate.query(sql, new TranactionTypeRowMapper());
		return txnType;
	}
}