package com.wwt.notification.dao;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.wwt.notification.model.AssingedTasks;
import com.wwt.notification.util.enums.WorkFlowActionType;


@RepositoryRestResource(collectionResourceRel = "wfTaskDocument", path = "wfTaskDocument")
public interface TaskDocumentDao
{
	/**
	 * This will get all the list of assigned tasks
	 * @param workflowId
	 * @param actionType
	 * @return list of AssignedTasks objects.
	 */
	public List<AssingedTasks> getListOfActiveTasks(String workflowId, WorkFlowActionType actionType);
}

