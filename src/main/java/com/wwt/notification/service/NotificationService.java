package com.wwt.notification.service;

import java.util.List;

import com.wwt.notification.model.WFEmailNotification;
import com.wwt.notification.model.AssingedTasks;
import com.wwt.notification.model.WFTransactionType;
import com.wwt.notification.util.enums.WorkFlowActionType;
import com.wwt.notification.vo.TransactionTypeVO;
import com.wwt.notification.vo.WFDocumentVO;

public interface NotificationService {

	public void saveNotification(WFDocumentVO doc, WorkFlowActionType actionType, int noOfEmailSent);
	public void updateNotification(WFEmailNotification emailNotification);
	public WFEmailNotification retrieveEmailNotification(WFDocumentVO doc, String actionType);
	public List<AssingedTasks> retrieveActiveWFTasks(String workflowId, WorkFlowActionType actionType);
	public List<WFTransactionType> getAllValidTransactions();
	public List<WFDocumentVO> findActiveWFDocs();
}
