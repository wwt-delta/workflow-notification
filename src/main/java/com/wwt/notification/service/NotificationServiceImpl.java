package com.wwt.notification.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.wwt.notification.dao.DocumentDao;
import com.wwt.notification.dao.EmailNotificationDao;
import com.wwt.notification.dao.TaskDocumentDao;
import com.wwt.notification.dao.impl.TransactionDataDaoImpl;
import com.wwt.notification.model.WFEmailNotification;
import com.wwt.notification.model.AssingedTasks;
import com.wwt.notification.model.WFTransactionType;
import com.wwt.notification.util.enums.WorkFlowActionType;
import com.wwt.notification.vo.WFDocumentVO;

@Service
@Transactional
public class NotificationServiceImpl implements NotificationService {
@Autowired
JdbcTemplate jdbcTemplate;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private EmailNotificationDao emailNotifcRepo;
	
	@Autowired
	private TransactionDataDaoImpl transRepo;
	
	@Autowired
	private DocumentDao documentDao;
	
	@Autowired
	private TaskDocumentDao taskRepo;
	
	@Override
	public void saveNotification(WFDocumentVO doc, WorkFlowActionType actionType, int noOfEmailSent) {
		WFEmailNotification emailNotification = new WFEmailNotification();
		try
		{
			String sql = "insert into emailnotification (application_name, client_id, last_email_sent_datetime,no_of_emails_sent,workflowid,actiontype,transactiontype) VALUES (?,?,?,?,?,?,?)";
			jdbcTemplate.update(sql,new Object[]{doc.getApplication(),doc.getClientId(),new Date(),noOfEmailSent,doc.getWorkflowId(),actionType.name(),doc.getTransactionType()});
		}
		catch(Exception ex)
		{
			logger.error("Error occured while saving the notification entry", ex);
		}
	}

	@Override
	public void updateNotification(WFEmailNotification emailNotification) {
    String sql="update  emailnotification set last_email_sent_datetime=?,no_of_emails_sent=? where id=?";
		jdbcTemplate.update(sql,new Object[]{emailNotification.getLastEmailTriggeredTime(),emailNotification.getNoOfEmailsSent(),emailNotification.getId()});
	}
	@Override
	public List<AssingedTasks> retrieveActiveWFTasks(String workflowId, WorkFlowActionType actionType) {
		return taskRepo.getListOfActiveTasks(workflowId, actionType);
	}
	@Override
	public List<WFTransactionType> getAllValidTransactions() {
		return transRepo.getAllValidTransactions();
	}
	@Override
	public List<WFDocumentVO> findActiveWFDocs() {
		return documentDao.findActiveWFDocuments();
	}
	@Override
	public WFEmailNotification retrieveEmailNotification(WFDocumentVO doc, String actionType) {
		return emailNotifcRepo.findEmailNotification(doc.getWorkflowId(), actionType);
	}
}