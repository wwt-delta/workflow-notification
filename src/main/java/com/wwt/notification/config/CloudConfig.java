package com.wwt.notification.config;

import javax.sql.DataSource;

import org.springframework.cloud.Cloud;
import org.springframework.cloud.CloudFactory;
import org.springframework.cloud.service.PooledServiceConnectorConfig;
import org.springframework.cloud.service.relational.DataSourceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Created by Delta on 23-03-2017.
 */
@Configuration
@Profile("cloud")
public class CloudConfig {

    @Bean
    public Cloud cloud() {
        CloudFactory cloudFactory = new CloudFactory();
        return cloudFactory.getCloud();
    }

    @Bean
    public DataSource dataSource() {
        PooledServiceConnectorConfig.PoolConfig poolConfig = new PooledServiceConnectorConfig.PoolConfig(1, 2, 3000);
        new PooledServiceConnectorConfig(poolConfig);
        DataSourceConfig dbConfig = new DataSourceConfig(poolConfig, null);
        return cloud().getServiceConnector("mysql", DataSource.class, dbConfig);
    }
}
