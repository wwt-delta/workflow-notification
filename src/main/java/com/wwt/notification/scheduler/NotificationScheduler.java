package com.wwt.notification.scheduler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wwt.notification.model.WFEmailNotification;
import com.wwt.notification.service.NotificationService;
import com.wwt.notification.util.enums.WorkFlowActionType;
import com.wwt.notification.utility.NotificationUtility;
import com.wwt.notification.vo.WFDocumentVO;


/**
 * To send email notifications to the pending tasks of REQUEST_APPROVAL, ASSIGN
 * and REQUEST_REVIEW task types every 10 minutes scheduler will poll the
 * pending tasks and send email to the users.
 * 
 * Also main notification settings are based on the transaction types and their
 * document priorities which is configurable in a configuration.properties file.
 * 
 * @author Delta Technology
 *
 */
@Component
@EnableScheduling
@PropertySource("classpath:application.yml")
public class NotificationScheduler {

	@Autowired
	private NotificationUtility notificationUtility;

	@Autowired
	private NotificationService notificationService;

	private static List<WorkFlowActionType> liWFActionTypes = new ArrayList<WorkFlowActionType>();

	static {
		liWFActionTypes.add(WorkFlowActionType.REQUEST_APPROVAL);
		liWFActionTypes.add(WorkFlowActionType.ASSIGN);
		liWFActionTypes.add(WorkFlowActionType.REVIEW);
	}

	/**
	 * Scheduler to run every 10 minutes to send email notification
	 */
	@Scheduled(cron = "${notification.mail.scheduletime}")
	public void sendEmailNotification() {
		Date currDate = new Date();
		System.out.println("scheduler started....");
		List<WFDocumentVO> documentList = notificationService.findActiveWFDocs();
		for (WFDocumentVO document : documentList) 
		{
			liWFActionTypes.forEach(action -> {
				WFEmailNotification emailNotification = notificationService.retrieveEmailNotification(document, action.name());
				if (null == emailNotification) 
				{
					int statusCode = notificationUtility.triggerEmailUtility(document, 1, action.name());
					if (statusCode == 201 || statusCode == 200) 
					{
						notificationService.saveNotification(document, action, 1);
					}
				}
				else 
				{
					if (document.getRecurringMailNotification() == 1) 
					{
						if (notificationUtility.isMailToSend(currDate.getTime(), emailNotification.getLastEmailTriggeredTime().getTime(), document.getPriority())) 
						{
							int statusCode = notificationUtility.triggerEmailUtility(document, emailNotification.getNoOfEmailsSent() + 1, action.name());
							if (statusCode == 201 || statusCode == 200) {
								notificationService.updateNotification(emailNotification);
							}
						}
					}
				}
			});
		}
	}
}