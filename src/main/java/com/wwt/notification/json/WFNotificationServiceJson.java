package com.wwt.notification.json;

import java.util.List;

public class WFNotificationServiceJson {
	
	private String ApplicationName;
	private int Priority;
	private List<String> TO;
	private List<String> CC;
	private List<String> BCC;
	private String From;
	private String Subject;
	private String Message;
	private boolean Retry;
	private int RetryCount;
	public String getApplicationName() {
		return ApplicationName;
	}
	public void setApplicationName(String applicationName) {
		ApplicationName = applicationName;
	}
	public int getPriority() {
		return Priority;
	}
	public void setPriority(int priority) {
		Priority = priority;
	}
	public List<String> getTO() {
		return TO;
	}
	public void setTO(List<String> tO) {
		TO = tO;
	}
	public List<String> getCC() {
		return CC;
	}
	public void setCC(List<String> cC) {
		CC = cC;
	}
	public List<String> getBCC() {
		return BCC;
	}
	public void setBCC(List<String> bCC) {
		BCC = bCC;
	}
	public String getFrom() {
		return From;
	}
	public void setFrom(String from) {
		From = from;
	}
	public String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public boolean isRetry() {
		return Retry;
	}
	public void setRetry(boolean retry) {
		Retry = retry;
	}
	public int getRetryCount() {
		return RetryCount;
	}
	public void setRetryCount(int retryCount) {
		RetryCount = retryCount;
	}
	@Override
	public String toString() {
		return "WFNotificationServiceJson [ApplicationName=" + ApplicationName + ", Priority=" + Priority + ", TO=" + TO
				+ ", CC=" + CC + ", BCC=" + BCC + ", From=" + From + ", Subject=" + Subject + ", Message=" + Message
				+ ", Retry=" + Retry + ", RetryCount=" + RetryCount + "]";
	}
	
}